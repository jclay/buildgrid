# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import random
import time
import uuid

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    Digest,
    FindMissingBlobsRequest
)
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2_grpc import ContentAddressableStorageStub
from buildgrid._protos.google.bytestream.bytestream_pb2 import WriteRequest
from buildgrid._protos.google.bytestream.bytestream_pb2_grpc import ByteStreamStub
from buildgrid.settings import HASH, MAX_REQUEST_SIZE
from locust import between, task

from common.grpc.client import RequestType
from common.grpc.user import GrpcUser


class CasGrpcUser(GrpcUser):
    abstract = False
    host = "localhost:50052"
    stub_classes = {
        "bytestream": ByteStreamStub,
        "cas": ContentAddressableStorageStub
    }
    wait_time = between(1, 5)

    def _gen_random_blob(self, size_bytes: int) -> bytes:
        blob = os.urandom(size_bytes)
        return blob

    def _make_digest(self, blob: bytes) -> Digest:
        return Digest(hash=HASH(blob).hexdigest(), size_bytes=len(blob))

    def _upload_blob(self, blob: bytes, digest: Digest, instance_name: str) -> None:
        def _generate_write_requests():
            offset = 0
            finished = False
            remaining = len(blob)
            transaction_id = str(uuid.uuid4())
            while not finished:
                chunk_size = min(remaining, MAX_REQUEST_SIZE)
                remaining -= chunk_size

                request = WriteRequest()
                request.resource_name = '/'.join([
                    instance_name,
                    'uploads',
                    transaction_id,
                    'blobs',
                    digest.hash,
                    str(digest.size_bytes)
                ])
                request.data = blob[offset:offset + chunk_size]
                request.write_offset = offset
                request.finish_write = remaining <= 0

                yield request

                offset += chunk_size
                finished = request.finish_write

        self.bytestream_client.Write(
            RequestType.STREAM_UNARY, _generate_write_requests())

    @task
    def find_missing_blobs(self) -> None:
        digests = []
        digest_count = random.randint(10, 100)
        hit_rate = 0.25
        blob_size = 120
        instance_name = ""

        for i in range(0, digest_count):
            blob = self._gen_random_blob(blob_size)
            digest = self._make_digest(blob)
            if i < hit_rate * digest_count:
                self._upload_blob(blob, digest, instance_name)
            digests.append(digest)

        request = FindMissingBlobsRequest(
            instance_name=instance_name, blob_digests=digests)
        self.cas_client.FindMissingBlobs(RequestType.UNARY_UNARY, request)
        time.sleep(1)
