# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from enum import Enum
import time
from typing import Callable, Dict

import grpc
from locust import events


class RequestType(Enum):
    UNARY_UNARY = 0
    UNARY_STREAM = 1
    STREAM_UNARY = 2
    STREAM_STREAM = 3


class GrpcClient:
    def __init__(self, stub):
        self._stub_class = stub.__class__
        self._stub = stub

    def _simple_request(
        self,
        func: Callable,
        request_meta: Dict,
        *args,
        **kwargs
    ) -> None:
        start_perf_counter = time.perf_counter()

        success = False
        try:
            request_meta["response"] = func(*args, **kwargs)
            success = True
        except grpc.RpcError as e:
            request_meta["exception"] = e

        request_meta["response_time"] = (time.perf_counter() - start_perf_counter) * 1000
        if success:
            request_meta["response_length"] = len(request_meta["response"].SerializeToString())

    def __getattr__(self, name: str) -> Callable:
        func = self._stub_class.__getattribute__(self._stub, name)

        def wrapper(request_type: RequestType, *args, **kwargs):
            request_meta = {
                "request_type": "grpc",
                "name": name,
                "start_time": time.time(),
                "response_length": 0,
                "exception": None,
                "context": None,
                "response": None,
            }

            if request_type in (RequestType.UNARY_UNARY, RequestType.STREAM_UNARY):
                self._simple_request(func, request_meta, *args, **kwargs)

            events.request.fire(**request_meta)
            return request_meta["response"]

        return wrapper
