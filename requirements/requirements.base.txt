#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --no-emit-trusted-host --no-index requirements.base.in
#
alembic==1.4.2            # via -r requirements.base.in
attrs==19.3.0             # via jsonschema
boto3==1.13.7             # via -r requirements.base.in
botocore==1.16.7          # via -r requirements.base.in, boto3, s3transfer
click==7.1.2              # via -r requirements.base.in
docutils==0.15.2          # via botocore
grpcio-reflection==1.28.1  # via -r requirements.base.in
grpcio==1.35.0            # via -r requirements.base.in, grpcio-reflection
janus==0.4.0              # via -r requirements.base.in
jmespath==0.9.5           # via boto3, botocore
jsonschema==3.2.0         # via -r requirements.base.in
lark-parser==0.9.0        # via -r requirements.base.in
mako==1.1.2               # via alembic
markupsafe==1.1.1         # via mako
protobuf==3.13.0          # via -r requirements.base.in, grpcio-reflection
pyrsistent==0.16.0        # via jsonschema
python-dateutil==2.8.1    # via alembic, botocore
python-editor==1.0.4      # via alembic
pyyaml==5.3.1             # via -r requirements.base.in
s3transfer==0.3.3         # via boto3
six==1.14.0               # via grpcio, jsonschema, protobuf, pyrsistent, python-dateutil
sqlalchemy==1.3.16        # via -r requirements.base.in, alembic
urllib3==1.25.9           # via botocore

# The following packages are considered to be unsafe in a requirements file:
# setuptools
