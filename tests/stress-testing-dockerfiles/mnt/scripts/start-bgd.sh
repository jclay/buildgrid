#!/bin/bash
# This is the BuildGrid start script

source /mnt/scripts/defaults.sh

config="/mnt/configs/${STRESS_TESTING_BGD_CONFIG:-bgd-sqlite.yml}"

wait_for_pges_up_and_ready_if_config "$config"

echo "Starting buildgrid with config: [$config]"

sleep ${STRESS_TESTING_GRACE_PERIOD:-10}

/app/env/bin/bgd server start -vv "$config"
