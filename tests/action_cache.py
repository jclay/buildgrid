# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


from unittest.mock import patch

import boto3
from botocore.exceptions import ClientError
import fakeredis
import io
import grpc
import pytest
from unittest.mock import patch

from buildgrid._exceptions import NotFoundError, StorageFullError
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._enums import ActionCacheEntryType
from buildgrid.server.actioncache.caches.lru_cache import LruActionCache
from buildgrid.server.actioncache.caches.remote_cache import RemoteActionCache
from buildgrid.server.actioncache.caches.s3_cache import S3ActionCache
from buildgrid.server.actioncache.caches.write_once_cache import WriteOnceActionCache
from buildgrid.server.cas.storage import lru_memory_cache
from moto import mock_s3

from .utils.action_cache import serve_cache
from .utils.utils import run_in_subprocess


@pytest.fixture
def cas():
    return lru_memory_cache.LRUMemoryCache(1024 * 1024)


def test_null_cas_action_cache(cas):
    cache = LruActionCache(cas, 0)

    action_digest1 = remote_execution_pb2.Digest(hash='alpha', size_bytes=4)
    dummy_result = remote_execution_pb2.ActionResult()

    cache.update_action_result(action_digest1, dummy_result)
    with pytest.raises(NotFoundError):
        cache.get_action_result(action_digest1)


def test_expiry(cas):
    cache = LruActionCache(cas, 2)

    action_digest1 = remote_execution_pb2.Digest(hash='alpha', size_bytes=4)
    action_digest2 = remote_execution_pb2.Digest(hash='bravo', size_bytes=4)
    action_digest3 = remote_execution_pb2.Digest(hash='charlie', size_bytes=4)
    dummy_result = remote_execution_pb2.ActionResult()

    cache.update_action_result(action_digest1, dummy_result)
    cache.update_action_result(action_digest2, dummy_result)

    # Get digest 1 (making 2 the least recently used)
    assert cache.get_action_result(action_digest1) is not None
    # Add digest 3 (so 2 gets removed from the cache)
    cache.update_action_result(action_digest3, dummy_result)

    assert cache.get_action_result(action_digest1) is not None
    with pytest.raises(NotFoundError):
        cache.get_action_result(action_digest2)

    assert cache.get_action_result(action_digest3) is not None


@pytest.mark.parametrize('acType', ['memory', 's3', 'write_once', 'redis'])
@mock_s3
def test_checks_cas(acType, cas):
    if acType == 'memory':
        cache = LruActionCache(cas, 50)
    elif acType == 's3':
        auth_args = {"aws_access_key_id": "access_key",
                     "aws_secret_access_key": "secret_key"}
        boto3.resource('s3', **auth_args).create_bucket(Bucket='cachebucket')
        cache = S3ActionCache(cas, allow_updates=True, cache_failed_actions=True, bucket='cachebucket',
                              access_key="access_key", secret_key="secret_key")
    elif acType == 'write_once':
        underlying_cache = LruActionCache(cas, 50)
        cache = WriteOnceActionCache(underlying_cache)
    elif acType == 'redis':
        with patch('buildgrid.server.actioncache.caches.redis_cache.redis.Redis', fakeredis.FakeRedis):
            from buildgrid.server.actioncache.caches.redis_cache import RedisActionCache
            cache = RedisActionCache(cas, True, True, 'localhost', 8000)

    action1 = remote_execution_pb2.Action(salt=b'action1')
    action_digest1 = cas.put_message(action1)

    action2 = remote_execution_pb2.Action(salt=b'action2')
    action_digest2 = cas.put_message(action2)

    action3 = remote_execution_pb2.Action(salt=b'action3')
    action_digest3 = cas.put_message(action3)

    # Create a tree that actions digests in CAS
    sample_digest = cas.put_message(remote_execution_pb2.Command(arguments=["sample"]))
    tree = remote_execution_pb2.Tree()
    tree.root.files.add().digest.CopyFrom(sample_digest)
    tree.children.add().files.add().digest.CopyFrom(sample_digest)
    tree_digest = cas.put_message(tree)

    # Add an ActionResult that actions real digests to the cache
    action_result1 = remote_execution_pb2.ActionResult()
    action_result1.output_directories.add().tree_digest.CopyFrom(tree_digest)
    action_result1.output_files.add().digest.CopyFrom(sample_digest)
    action_result1.stdout_digest.CopyFrom(sample_digest)
    action_result1.stderr_digest.CopyFrom(sample_digest)
    cache.update_action_result(action_digest1, action_result1)

    # Add ActionResults that action fake digests to the cache
    action_result2 = remote_execution_pb2.ActionResult()
    action_result2.output_directories.add().tree_digest.hash = "nonexistent"
    action_result2.output_directories[0].tree_digest.size_bytes = 8
    cache.update_action_result(action_digest2, action_result2)

    action_result3 = remote_execution_pb2.ActionResult()
    action_result3.stdout_digest.hash = "nonexistent"
    action_result3.stdout_digest.size_bytes = 8
    cache.update_action_result(action_digest3, action_result3)

    # Verify we can get the first ActionResult but not the others
    fetched_result1 = cache.get_action_result(action_digest1)
    assert fetched_result1.output_directories[0].tree_digest.hash == tree_digest.hash
    with pytest.raises(NotFoundError):
        cache.get_action_result(action_digest2)
        cache.get_action_result(action_digest3)


def test_remote_update():

    def __test_update():
        with serve_cache(['testing']) as server:
            channel = grpc.insecure_channel(server.remote)
            cache = RemoteActionCache(channel, 'testing')

            action_digest = remote_execution_pb2.Digest(hash='alpha', size_bytes=4)
            result = remote_execution_pb2.ActionResult()
            cache.update_action_result(action_digest, result)

            fetched = cache.get_action_result(action_digest)
            assert result == fetched
        return True

    assert run_in_subprocess(__test_update)


def test_remote_update_disallowed():

    def __test_update_disallowed():
        with serve_cache(['testing'], allow_updates=False) as server:
            channel = grpc.insecure_channel(server.remote)
            cache = RemoteActionCache(channel, 'testing')

            action_digest = remote_execution_pb2.Digest(hash='alpha', size_bytes=4)
            result = remote_execution_pb2.ActionResult()
            with pytest.raises(NotImplementedError, match='Updating cache not allowed'):
                cache.update_action_result(action_digest, result)
        return True

    assert run_in_subprocess(__test_update_disallowed)


def test_remote_get_missing():

    def __test_get_missing():
        with serve_cache(['testing']) as server:
            channel = grpc.insecure_channel(server.remote)
            cache = RemoteActionCache(channel, 'testing')

            action_digest = remote_execution_pb2.Digest(hash='alpha', size_bytes=4)
            with pytest.raises(NotFoundError):
                cache.get_action_result(action_digest)
        return True

    assert run_in_subprocess(__test_get_missing)


@mock_s3
def test_full_s3_action_cache(cas):

    def __test_action_cache_write():
        action_digest = remote_execution_pb2.Digest(hash='alpha', size_bytes=4)
        result = remote_execution_pb2.ActionResult()
        with pytest.raises(StorageFullError):
            cache.update_action_result(action_digest, result)

    auth_args = {"aws_access_key_id": "access_key",
                 "aws_secret_access_key": "secret_key"}
    boto3.resource('s3', **auth_args).create_bucket(Bucket='cachebucket')
    cache = S3ActionCache(cas, allow_updates=True, cache_failed_actions=True, bucket='cachebucket',
                          access_key="access_key", secret_key="secret_key")

    with patch('botocore.client.BaseClient._make_api_call') as m:
        response = {'Error': {'Code': 'QuotaExceededException'}}
        err = ClientError(response, "Error")
        m.side_effect = err
        __test_action_cache_write()

@mock_s3
@pytest.mark.parametrize('entryType', [ActionCacheEntryType.ACTION_RESULT_DIGEST,
                                       ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize('migrateEntries', [False, True])
def test_s3_hits(cas, entryType, migrateEntries):
    access_key = "access_key"
    secret_key = "secret_key"

    auth_args = {"aws_access_key_id": access_key,
                 "aws_secret_access_key": secret_key}

    bucket_name = 'cachebucket'
    boto3.resource('s3', **auth_args).create_bucket(Bucket=bucket_name)

    cache = S3ActionCache(cas, allow_updates=True, cache_failed_actions=True,
                          entry_type=entryType,
                          migrate_entries=migrateEntries,
                          bucket=bucket_name, access_key=access_key,
                          secret_key=secret_key)

    action = remote_execution_pb2.Action(salt=b'action')
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    action_result_digest = cas.put_message(action_result)

    s3_cache_key = f'{action_digest.hash}_{action_digest.size_bytes}'
    obj = cache._s3cache.Object(bucket_name, s3_cache_key)

    # Depending on the configuration of the cache, the entries in S3
    # will contain the Digest or the `ActionResult` itself.
    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        obj.upload_fileobj(io.BytesIO(action_result_digest.SerializeToString()))
    else:
        obj.upload_fileobj(io.BytesIO(action_result.SerializeToString()))

    # When querying the cache we get a hit:
    cached_action_result = cache.get_action_result(action_digest)
    assert cached_action_result == action_result

    # And we also get hits for the other entry type:
    if entryType == ActionCacheEntryType.ACTION_RESULT:
        obj.upload_fileobj(io.BytesIO(action_result_digest.SerializeToString()))
    else:
        obj.upload_fileobj(io.BytesIO(action_result.SerializeToString()))

    assert cache.get_action_result(action_digest) == action_result

@mock_s3
@pytest.mark.parametrize('entryType', [ActionCacheEntryType.ACTION_RESULT_DIGEST,
                                       ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize('migrateEntries', [False, True])
def test_s3_writes(cas, entryType, migrateEntries):
    access_key = "access_key"
    secret_key = "secret_key"

    auth_args = {"aws_access_key_id": access_key,
                 "aws_secret_access_key": secret_key}

    bucket_name = 'cachebucket'
    boto3.resource('s3', **auth_args).create_bucket(Bucket=bucket_name)

    cache = S3ActionCache(cas, allow_updates=True, cache_failed_actions=True,
                          entry_type=entryType,
                          migrate_entries=migrateEntries,
                          bucket=bucket_name, access_key=access_key,
                          secret_key=secret_key)

    action = remote_execution_pb2.Action(salt=b'action')
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    action_result_digest = cas.put_message(action_result)

    cache.update_action_result(action_digest, action_result)
    assert cache.get_action_result(action_digest) == action_result

    # The `ActionResult` was added to the underlying storage:
    assert cas.get_message(action_result_digest, remote_execution_pb2.ActionResult) == action_result

    # Checking the entry created in S3:
    s3_cache_key = f'{action_digest.hash}_{action_digest.size_bytes}'
    obj = cache._s3cache.Object(bucket_name, s3_cache_key)
    s3_cache_entry = obj.get()['Body'].read()

    # Depending on the cache configuration, the entry should contain
    # either the `ActionResult` or its digest.
    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        assert s3_cache_entry == action_result_digest.SerializeToString()
    else:
        assert s3_cache_entry == action_result.SerializeToString()

@mock_s3
@pytest.mark.parametrize('entryType', [ActionCacheEntryType.ACTION_RESULT_DIGEST,
                                       ActionCacheEntryType.ACTION_RESULT])
def test_s3_migrate_entries_to_new_type(cas, entryType):
    access_key = "access_key"
    secret_key = "secret_key"

    auth_args = {"aws_access_key_id": access_key,
                 "aws_secret_access_key": secret_key}

    bucket_name = 'cachebucket'
    boto3.resource('s3', **auth_args).create_bucket(Bucket=bucket_name)

    cache = S3ActionCache(cas, allow_updates=True, cache_failed_actions=True,
                          entry_type=entryType,
                          migrate_entries=True,
                          bucket=bucket_name, access_key=access_key,
                          secret_key=secret_key)

    action = remote_execution_pb2.Action(salt=b'action')
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    action_result_digest = cas.put_message(action_result)

    # Storing an entry created in S3 with the old type:
    s3_cache_key = f'{action_digest.hash}_{action_digest.size_bytes}'
    obj = cache._s3cache.Object(bucket_name, s3_cache_key)

    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        entry_value = action_result.SerializeToString()
    else:
        entry_value = action_result_digest.SerializeToString()

    obj.upload_fileobj(io.BytesIO(entry_value))

    # Accessing the entry:
    assert cache.get_action_result(action_digest) is not None

    # Depending on the cache configuration, the entry should have been
    # migrated to the new selected format (`entry_type`)
    s3_cache_entry = obj.get()['Body'].read()
    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        assert s3_cache_entry == action_result_digest.SerializeToString()
    else:
        assert s3_cache_entry == action_result.SerializeToString()


@pytest.mark.parametrize('entryType', [ActionCacheEntryType.ACTION_RESULT_DIGEST,
                                       ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize('migrateEntries', [False, True])
def test_redis_misses(cas, entryType, migrateEntries):
    with patch('buildgrid.server.actioncache.caches.redis_cache.redis.Redis', fakeredis.FakeRedis):
        from buildgrid.server.actioncache.caches.redis_cache import RedisActionCache
        cache = RedisActionCache(storage=cas, allow_updates=True,
                                 cache_failed_actions=True,
                                 entry_type=entryType,
                                 migrate_entries=migrateEntries,
                                 host='localhost', port=8000)

        digest = remote_execution_pb2.Digest(hash='foo', size_bytes=123)
        with pytest.raises(NotFoundError):
            cache.get_action_result(digest)

@pytest.mark.parametrize('entryType', [ActionCacheEntryType.ACTION_RESULT_DIGEST,
                                       ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize('migrateEntries', [False, True])
def test_redis_hits(cas, entryType, migrateEntries):
    with patch('buildgrid.server.actioncache.caches.redis_cache.redis.Redis', fakeredis.FakeRedis):
        from buildgrid.server.actioncache.caches.redis_cache import RedisActionCache
        cache = RedisActionCache(storage=cas, allow_updates=True,
                                 cache_failed_actions=True,
                                 entry_type=entryType,
                                 migrate_entries=migrateEntries,
                                 host='localhost', port=8000)

        action = remote_execution_pb2.Action(salt=b'action')
        action_digest = cas.put_message(action)

        # Populating the CAS with a non-empty `ActionResult`:
        action_result = remote_execution_pb2.ActionResult(exit_code=42)
        action_result_digest = cas.put_message(action_result)

        redis_cache_key = f'action-cache.{action_digest.hash}_{action_digest.size_bytes}'

        # Depending on the configuration of the cache, the entries in S3
        # will contain the Digest or the `ActionResult` itself.
        if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
            cache._client.set(redis_cache_key, action_result_digest.SerializeToString())
        else:
            cache._client.set(redis_cache_key, action_result.SerializeToString())

        # When querying the cache we get a hit:
        cached_action_result = cache.get_action_result(action_digest)
        assert cached_action_result == action_result

        # And we also get hits for the other entry type:
        if entryType == ActionCacheEntryType.ACTION_RESULT:
            cache._client.set(redis_cache_key ,action_result_digest.SerializeToString())
        else:
            cache._client.set(redis_cache_key, action_result.SerializeToString())

        assert cache.get_action_result(action_digest) == action_result


@pytest.mark.parametrize('entryType', [ActionCacheEntryType.ACTION_RESULT_DIGEST,
                                       ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize('migrateEntries', [False, True])
def test_redis_writes(cas, entryType, migrateEntries):
    with patch('buildgrid.server.actioncache.caches.redis_cache.redis.Redis', fakeredis.FakeRedis):
        from buildgrid.server.actioncache.caches.redis_cache import RedisActionCache
        cache = RedisActionCache(storage=cas, allow_updates=True,
                                 cache_failed_actions=True,
                                 entry_type=entryType,
                                 migrate_entries=migrateEntries,
                                 host='localhost', port=8000)

        action = remote_execution_pb2.Action(salt=b'action')
        action_digest = cas.put_message(action)

        # Populating the CAS with a non-empty `ActionResult`:
        action_result = remote_execution_pb2.ActionResult(exit_code=42)
        action_result_digest = cas.put_message(action_result)

        cache.update_action_result(action_digest, action_result)
        assert cache.get_action_result(action_digest) == action_result

        # The `ActionResult` was added to the underlying storage:
        assert cas.get_message(action_result_digest, remote_execution_pb2.ActionResult) == action_result

        # Checking the entry created in Redis:
        redis_cache_key = f'action-cache.{action_digest.hash}_{action_digest.size_bytes}'
        redis_cache_value = cache._client.get(redis_cache_key)

        # Depending on the cache configuration, the entry should contain
        # either the `ActionResult` or its digest.
        if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
            assert redis_cache_value == action_result_digest.SerializeToString()
        else:
            assert redis_cache_value == action_result.SerializeToString()


@pytest.mark.parametrize('entryType', [ActionCacheEntryType.ACTION_RESULT_DIGEST,
                                       ActionCacheEntryType.ACTION_RESULT])
def test_redis_migrate_entries_to_new_type(cas, entryType):
    with patch('buildgrid.server.actioncache.caches.redis_cache.redis.Redis', fakeredis.FakeRedis):
        from buildgrid.server.actioncache.caches.redis_cache import RedisActionCache
        cache = RedisActionCache(storage=cas, allow_updates=True,
                                 cache_failed_actions=True,
                                 entry_type=entryType,
                                 migrate_entries=True,
                                 host='localhost', port=8000)

        action = remote_execution_pb2.Action(salt=b'action')
        action_digest = cas.put_message(action)

        # Populating the CAS with a non-empty `ActionResult`:
        action_result = remote_execution_pb2.ActionResult(exit_code=42)
        action_result_digest = cas.put_message(action_result)

        # Storing an entry created in Redis with the old type:
        redis_cache_key = f'action-cache.{action_digest.hash}_{action_digest.size_bytes}'
        if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
            entry_value = action_result.SerializeToString()
        else:
            entry_value = action_result_digest.SerializeToString()
        cache._client.set(redis_cache_key, entry_value)

        # Accessing the entry:
        assert cache.get_action_result(action_digest) is not None

        # Depending on the cache configuration, the entry should have been
        # migrated to the new selected format (`entry_type`)
        s3_cache_entry = cache._client.get(redis_cache_key)
        if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
            assert s3_cache_entry == action_result_digest.SerializeToString()
        else:
            assert s3_cache_entry == action_result.SerializeToString()

