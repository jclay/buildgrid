# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from buildgrid._app.bots.utils import command_output_paths
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Command


def test_command_with_output_paths():
    command = Command()
    command.output_paths.extend(['dir1/dir2', 'dir3'])

    assert list(command_output_paths(command)) == command.output_paths


def test_command_with_output_files_and_directories():
    command = Command()
    command.output_directories.extend(['dirA', 'dirB'])
    command.output_files.extend(['file1.txt', 'a.out'])

    assert list(command_output_paths(command)) == (list(command.output_files) + list(command.output_directories))


def test_command_with_both_output_fields_set():
    # REAPI >= v2.1
    command = Command()
    command.output_paths.extend(['output_path1', 'output_path2'])

    # According to the REAPI spec, these two fields are ignored.
    command.output_directories.extend(['dir1/dir2', 'dir3'])
    command.output_files.extend(['file1.txt', 'a.out'])

    assert list(command_output_paths(command)) == command.output_paths
