# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import bisect
import logging
from multiprocessing import Queue
from threading import Lock, Thread
from typing import List, Tuple
from datetime import datetime
import time

from buildgrid._protos.google.longrunning import operations_pb2
from buildgrid._enums import LeaseState, MetricCategories, OperationStage
from buildgrid._exceptions import InvalidArgumentError
from buildgrid.utils import Condition, JobState, BrowserURL
from buildgrid.settings import MAX_JOB_BLOCK_TIME
from buildgrid.server.metrics_names import (
    BOTS_ASSIGN_JOB_LEASES_TIME_METRIC_NAME,
    DATA_STORE_CHECK_FOR_UPDATE_TIME_METRIC_NAME,
    DATA_STORE_CREATE_JOB_TIME_METRIC_NAME,
    DATA_STORE_CREATE_LEASE_TIME_METRIC_NAME,
    DATA_STORE_CREATE_OPERATION_TIME_METRIC_NAME,
    DATA_STORE_GET_JOB_BY_DIGEST_TIME_METRIC_NAME,
    DATA_STORE_GET_JOB_BY_NAME_TIME_METRIC_NAME,
    DATA_STORE_GET_JOB_BY_OPERATION_TIME_METRIC_NAME,
    DATA_STORE_LIST_OPERATIONS_TIME_METRIC_NAME,
    DATA_STORE_QUEUE_JOB_TIME_METRIC_NAME,
    DATA_STORE_UPDATE_JOB_TIME_METRIC_NAME,
    DATA_STORE_UPDATE_LEASE_TIME_METRIC_NAME,
    DATA_STORE_UPDATE_OPERATION_TIME_METRIC_NAME
)
from buildgrid.server.metrics_utils import DurationMetric
from buildgrid.server.operations.filtering import OperationFilter, DEFAULT_OPERATION_FILTERS
from buildgrid.server.persistence.interface import DataStoreInterface


class MemoryDataStore(DataStoreInterface):

    def __init__(self, storage):
        super().__init__()
        self.logger = logging.getLogger(__file__)
        self.logger.info("Creating in-memory scheduler")

        self.queue = []
        self.queue_lock = Lock()
        self.queue_condition = Condition(lock=self.queue_lock)

        self.jobs_by_action = {}
        self.jobs_by_operation = {}
        self.jobs_by_name = {}

        self.operations_by_stage = {}
        self.leases_by_state = {}
        self.is_instrumented = False

        self.storage = storage
        self.update_event_queue = Queue()
        self.watcher = Thread(name="JobWatcher", target=self.wait_for_job_updates, daemon=True)
        self.watcher_keep_running = True
        self.watcher.start()

    def __repr__(self):
        return "In-memory data store interface"

    def activate_monitoring(self):
        if self.is_instrumented:
            return

        self.operations_by_stage = {
            stage: set() for stage in OperationStage
        }
        self.leases_by_state = {
            state: set() for state in LeaseState
        }
        self.is_instrumented = True

    def deactivate_monitoring(self):
        if not self.is_instrumented:
            return

        self.operations_by_stage = {}
        self.leases_by_state = {}
        self.is_instrumented = False

    def _check_job_timeout(self, job_internal, *, max_execution_timeout=None):
        """ Do a lazy check of maximum allowed job timeouts when clients try to retrieve
            an existing job.
            Cancel the job and related operations/leases, if we detect they have
            exceeded timeouts on access.

            Returns the `buildgrid.server.Job` object, possibly updated with `cancelled=True`.
        """
        if job_internal and max_execution_timeout and job_internal.worker_start_timestamp_as_datetime:
            if job_internal.operation_stage == OperationStage.EXECUTING:
                executing_duration = datetime.utcnow() - job_internal.worker_start_timestamp_as_datetime
                if executing_duration.total_seconds() >= max_execution_timeout:
                    self.logger.warning(f"Job=[{job_internal}] has been executing for "
                                        f"executing_duration=[{executing_duration}]. "
                                        f"max_execution_timeout=[{max_execution_timeout}] "
                                        "Cancelling.")
                    job_internal.cancel_all_operations(data_store=self)
                    self.logger.info(f"Job=[{job_internal}] has been cancelled.")
        return job_internal

    @DurationMetric(DATA_STORE_GET_JOB_BY_NAME_TIME_METRIC_NAME, instanced=True)
    def get_job_by_name(self, name, *, max_execution_timeout=None):
        job = self.jobs_by_name.get(name)
        return self._check_job_timeout(job, max_execution_timeout=max_execution_timeout)

    @DurationMetric(DATA_STORE_GET_JOB_BY_DIGEST_TIME_METRIC_NAME, instanced=True)
    def get_job_by_action(self, action_digest, *, max_execution_timeout=None):
        job = self.jobs_by_action.get(action_digest.hash)
        return self._check_job_timeout(job, max_execution_timeout=max_execution_timeout)

    @DurationMetric(DATA_STORE_GET_JOB_BY_OPERATION_TIME_METRIC_NAME, instanced=True)
    def get_job_by_operation(self, name, *, max_execution_timeout=None):
        job = self.jobs_by_operation.get(name)
        return self._check_job_timeout(job, max_execution_timeout=max_execution_timeout)

    def get_all_jobs(self):
        return [job for job in self.jobs_by_name.values()
                if job.operation_stage != OperationStage.COMPLETED]

    def get_jobs_by_stage(self, operation_stage):
        return [job for job in self.jobs_by_name.values()
                if job.operation_stage == operation_stage]

    def _get_job_count_by_stage(self):
        results = []
        for stage in OperationStage:
            results.append((stage, len(self.get_jobs_by_stage(stage))))
        return results

    @DurationMetric(DATA_STORE_CREATE_JOB_TIME_METRIC_NAME, instanced=True)
    def create_job(self, job):
        self.jobs_by_action[job.action_digest.hash] = job
        self.jobs_by_name[job.name] = job
        if self._action_browser_url is not None:
            job.set_action_url(BrowserURL(self._action_browser_url, self._instance_name))

    @DurationMetric(DATA_STORE_QUEUE_JOB_TIME_METRIC_NAME, instanced=True)
    def queue_job(self, job_name):
        job = self.jobs_by_name[job_name]
        with self.queue_condition:
            if job.operation_stage != OperationStage.QUEUED:
                bisect.insort(self.queue, job)
                self.logger.info(f"Job queued: [{job.name}]")
            else:
                self.logger.info(f"Job already queued: [{job.name}]")
                self.queue.sort()

    @DurationMetric(DATA_STORE_UPDATE_JOB_TIME_METRIC_NAME, instanced=True)
    def update_job(self, job_name, changes, skip_notify=False):
        # With this implementation, there's no need to actually make
        # changes to the stored job, since its a reference to the
        # in-memory job that caused this method to be called.
        self.update_event_queue.put((job_name, changes, skip_notify))

    def delete_job(self, job_name):
        job = self.jobs_by_name[job_name]

        del self.jobs_by_action[job.action_digest.hash]
        del self.jobs_by_name[job.name]

        self.logger.info(f"Job deleted: [{job.name}]")

        if self.is_instrumented:
            for stage in OperationStage:
                self.operations_by_stage[stage].discard(job.name)

            for state in LeaseState:
                self.leases_by_state[state].discard(job.name)

    def wait_for_job_updates(self):
        self.logger.info("Starting job watcher thread")
        while self.watcher_keep_running:
            try:
                job_name, changes, skip_notify = self.update_event_queue.get()
            except EOFError:
                continue
            with DurationMetric(DATA_STORE_CHECK_FOR_UPDATE_TIME_METRIC_NAME):
                with self.watched_jobs_lock:
                    if (all(field not in changes for field in ("cancelled", "stage")) or
                            job_name not in self.watched_jobs):
                        # If the stage or cancellation state haven't changed, we don't
                        # need to do anything with this event. Similarly, if we aren't
                        # watching this job, we can ignore the event.
                        continue
                    job = self.get_job_by_name(job_name)
                    spec = self.watched_jobs[job_name]
                    new_state = JobState(job)
                    if spec.last_state != new_state and not skip_notify:
                        spec.last_state = new_state
                        if not skip_notify:
                            spec.event.notify_change()

    def store_response(self, job, commit_changes):
        # The job is always in memory in this implementation, so there's
        # no need to write anything to the CAS, since the job stays in
        # memory as long as we need it
        pass

    def get_operations_by_stage(self, operation_stage):
        return self.operations_by_stage.get(operation_stage, set())

    def _get_operation_count_by_stage(self):
        results = []
        for stage in OperationStage:
            results.append((stage, len(self.get_operations_by_stage(stage))))
        return results

    @DurationMetric(DATA_STORE_LIST_OPERATIONS_TIME_METRIC_NAME, instanced=True)
    def list_operations(self,
                        operation_filters: List[OperationFilter]=None,
                        page_size: int=None,
                        page_token: str=None,
                        max_execution_timeout: int=None) -> Tuple[List[operations_pb2.Operation], str]:

        if operation_filters and operation_filters != DEFAULT_OPERATION_FILTERS:
            raise InvalidArgumentError("Filtering is not supported with the in-memory scheduler.")

        if page_token:
            raise InvalidArgumentError("page_token is not supported in the in-memory scheduler.")

        # Run through all the jobs and see if any of are
        # exceeding the execution timeout; mark those as cancelled
        for job in self.jobs_by_name.values():
            self._check_job_timeout(job, max_execution_timeout=max_execution_timeout)

        # Return all operations
        return [
            operation for job in self.jobs_by_name.values() for operation in job.get_all_operations()
        ], ""

    @DurationMetric(DATA_STORE_CREATE_OPERATION_TIME_METRIC_NAME, instanced=True)
    def create_operation(self, operation_name, job_name, request_metadata=None):
        job = self.jobs_by_name[job_name]
        self.jobs_by_operation[operation_name] = job
        if self.is_instrumented:
            self.operations_by_stage[job.operation_stage].add(job_name)

    @DurationMetric(DATA_STORE_UPDATE_OPERATION_TIME_METRIC_NAME, instanced=True)
    def update_operation(self, operation_name, changes):
        if self.is_instrumented:
            job = self.jobs_by_operation[operation_name]
            self.operations_by_stage[job.operation_stage].add(job.name)
            other_stages = [member for member in OperationStage if member != job.operation_stage]
            for stage in other_stages:
                self.operations_by_stage[stage].discard(job.name)

    def delete_operation(self, operation_name):
        del self.jobs_by_operation[operation_name]

    def get_leases_by_state(self, lease_state):
        return self.leases_by_state.get(lease_state, set())

    def _get_lease_count_by_state(self):
        results = []
        for state in LeaseState:
            results.append((state, len(self.get_leases_by_state(state))))
        return results

    @DurationMetric(DATA_STORE_CREATE_LEASE_TIME_METRIC_NAME, instanced=True)
    def create_lease(self, lease):
        if self.is_instrumented:
            self.leases_by_state[LeaseState(lease.state)].add(lease.id)

    @DurationMetric(DATA_STORE_UPDATE_LEASE_TIME_METRIC_NAME, instanced=True)
    def update_lease(self, job_name, changes):
        if self.is_instrumented:
            job = self.jobs_by_name[job_name]
            state = LeaseState(job.lease.state)
            self.leases_by_state[state].add(job.lease.id)
            other_states = [member for member in LeaseState if member != state]
            for state in other_states:
                self.leases_by_state[state].discard(job.lease.id)

    def load_unfinished_jobs(self):
        return []

    def get_operation_request_metadata_by_name(self, operation_name):
        return None

    @DurationMetric(BOTS_ASSIGN_JOB_LEASES_TIME_METRIC_NAME, instanced=True)
    def assign_lease_for_next_job(self, capabilities, callback, timeout=None):
        """Return the highest priority job that can be run by a worker.

        Iterate over the job queue and find the highest priority job which
        the worker can run given the provided capabilities. Takes a
        dictionary of worker capabilities to compare with job requirements.

        :param capabilities: Dictionary of worker capabilities to compare
            with job requirements when finding a job.
        :type capabilities: dict
        :param callback: Function to run on the next runnable job, should return
            a list of leases.
        :type callback: function
        :param timeout: time to block waiting on job queue, caps if longer
            than MAX_JOB_BLOCK_TIME.
        :type timeout: int
        :returns: A job

        """
        if not timeout and not self.queue:
            return []

        with self.queue_condition:
            leases = self._assign_lease(capabilities, callback)

            self.queue_condition.notify()

            if timeout:
                # Cap the timeout if it's larger than MAX_JOB_BLOCK_TIME
                timeout = min(timeout, MAX_JOB_BLOCK_TIME)
                deadline = time.time() + timeout
                while not leases and time.time() < deadline:
                    ready = self.queue_condition.wait(timeout=deadline - time.time())
                    if not ready:
                        # If we ran out of time waiting for the condition variable,
                        # give up early.
                        break
                    leases = self._assign_lease(capabilities, callback, deadline=deadline)
                    self.queue_condition.notify()

        return leases

    def _assign_lease(self, worker_capabilities, callback, deadline=None):
        for index, job in enumerate(self.queue):
            if deadline is not None and time.time() >= deadline:
                break
            # Don't queue a cancelled job, it would be unable to get a lease anyway
            if job.cancelled:
                self.logger.debug(f"Dropping cancelled job: [{job.name}] from queue")
                del self.queue[index]
                continue

            if self._worker_is_capable(worker_capabilities, job):
                leases = callback(job)
                if leases:
                    del self.queue[index]
                    return leases
        return []

    def _worker_is_capable(self, worker_capabilities, job):
        """Returns whether the worker is suitable to run the job."""
        # TODO: Replace this with the logic defined in the Platform msg. standard.

        job_requirements = job.platform_requirements
        # For now we'll only check OS and ISA properties.

        if not job_requirements:
            return True

        for req, matches in job_requirements.items():
            if not matches <= worker_capabilities.get(req, set()):
                return False
        return True

    def get_metrics(self):
        metrics = {}
        metrics[MetricCategories.JOBS.value] = {
            stage.value: count for stage, count in self._get_job_count_by_stage()
        }
        metrics[MetricCategories.LEASES.value] = {
            state.value: count for state, count in self._get_lease_count_by_state()
        }

        return metrics
